public class Formulas {
    public static void main(String[] args) {
        System.out.println("Len of circe with radius 3:");
        System.out.println(lenByRadius(3));

        double lenOfCircle = lenByRadius(1);
        System.out.println("Len of circe with radius 1:");
        System.out.println(lenOfCircle);

        System.out.println("Radius of circle with len 6.28:");
        System.out.println(radiusByLen(6.28));

        //Проверим, что при вычислении радиуса по длине, полученной нашей функцией
        //получится тот же радиус, что мы задали
        System.out.println(radiusByLen(lenByRadius(1)));
    }

    //Вычисление длины окружности по радиусу
    public static double lenByRadius(double radius) {
        //Math.PI - взять число пи из стандартной математической библиотеки
        return Math.PI * 2 * radius;
    }

    //Вычисление радиуса окружности по длине
    public static double radiusByLen(double len) {
        return len / (2 * Math.PI);
    }
}
